﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace server
{
    public partial class FromServer : Form
    {
        private static Socket serverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        private static byte[] Buffer = new byte[1024];
        public FromServer()
        {
            InitializeComponent();
            StartServer();
        }
        

        private void StartServer()
        {
            serverSocket.Bind(new IPEndPoint(IPAddress.Any, 50000));
            serverSocket.Listen(2);
            serverSocket.BeginAccept(new AsyncCallback(waitForNewConnection), serverSocket);
        }
        private void waitForNewConnection(IAsyncResult AR)
        {
            Socket socket = serverSocket.EndAccept(AR);
            socket.BeginReceive(Buffer, 0, Buffer.Length, SocketFlags.None, new AsyncCallback(RecieveInformation), socket);
            serverSocket.BeginAccept(new AsyncCallback(waitForNewConnection), serverSocket);
        }
        private void RecieveInformation(IAsyncResult AR)
        {
            Socket socket = (Socket)AR.AsyncState;
            IPEndPoint remoteIpEndPoint = socket.RemoteEndPoint as IPEndPoint;
            IPEndPoint localIpEndPoint = socket.LocalEndPoint as IPEndPoint;
            //MessageBox.Show("client IP:" + remoteIpEndPoint.ToString() +
             //            "\n server IP:" + localIpEndPoint.ToString());
            int recieve = socket.EndReceive(AR);
            byte[] dataBuff = new byte[recieve];
            Array.Copy(Buffer, dataBuff, recieve);
            string text = Encoding.ASCII.GetString(dataBuff);
            string[] ClientInformation = text.Split(';');
           
            returnInformationToClient(socket, ClientInformation);
        }
        private void returnInformationToClient(Socket socket, string[] response)
        {
            string result = "";
            if (response[0].Equals("Calculation"))
            {
                double num1, num2;
                if (response.Length != 4 || !double.TryParse(response[2], out num1) || !double.TryParse(response[3], out num2) || (!response[1].Equals("+") && !response[1].Equals("-") && !response[1].Equals("*") && !response[1].Equals("^") && !response[1].Equals("/")))
                {
                    result = "You have a format problem!!!(format: <number> <command(Add/Sub/Mul/div/Pow)> <number>)";
                }
                else
                {
                    double calc = 0;
                    if (response[1].Equals("+")) calc = num1 + num2;
                    if (response[1].Equals("-")) calc = num1 - num2;
                    if (response[1].Equals("*")) calc = num1 * num2;
                    if (response[1].Equals("/")) calc = num1 / num2;
                    if (response[1].Equals("^")) calc = Math.Pow(num1, num2);
                    result = calc.ToString();
                }
            }
            if (response[0].Equals("Ellipse"))
            {
                int E_x, E_y, length, width, isPen;
                if (response.Length != 7 || !int.TryParse(response[1], out E_x) || !int.TryParse(response[2], out E_y) || !int.TryParse(response[3], out length) || !int.TryParse(response[4], out width) || !int.TryParse(response[5], out isPen) || (!response[6].Equals("Black") && !response[6].Equals("Green") && !response[6].Equals("Blue") && !response[6].Equals("Red") && !response[6].Equals("White")) || (isPen != 0 && isPen != 1))
                {
                    result = "You have a format problem!!!(format: x = number, y = number, length = number, width = number, Choice = 1/0, color = White/Red/Black/Blue/Green)";
                }
                else result = "The ellipse was successfully painted!!";
            }
            if (response[0].Equals("Rectangle"))
            {
                int R_x, R_y, length, width, isPen;
                if (response.Length != 7 || !int.TryParse(response[1], out R_x) || !int.TryParse(response[2], out R_y) || !int.TryParse(response[3], out length) || !int.TryParse(response[4], out width) || !int.TryParse(response[5], out isPen) || (!response[6].Equals("Black") && !response[6].Equals("Green") && !response[6].Equals("Blue") && !response[6].Equals("Red") && !response[6].Equals("White")) || (isPen != 0 && isPen != 1))
                {
                    result = "You have a format problem!!!(format: x = number, y = number, length = number, width = number, Choice = 1/0, color = White/Red/Black/Blue/Green)";
                }
                else result = "The rectangle was successfully painted!!";
            }
            if(response[0].Equals("Function"))
            {
                int X, Y, Incline;
                if (response.Length != 4 || !int.TryParse(response[1], out Incline) || !int.TryParse(response[2], out X) || !int.TryParse(response[3], out Y))
                {
                    result = "You have a format problem!!!(format: (point)4,5  (incline)number)";
                }
                else result = "The function draw successfully!!!";
            }
            Console.WriteLine("Sending back " + result);

            byte[] data = Encoding.ASCII.GetBytes(result);
            socket.BeginSend(data, 0, data.Length, SocketFlags.None, new AsyncCallback(EndRecieveInformation), socket);
            socket.BeginReceive(Buffer, 0, Buffer.Length, SocketFlags.None, new AsyncCallback(RecieveInformation), socket);
        }
        private static void EndRecieveInformation(IAsyncResult AR)
        {
            Socket socket = (Socket)AR.AsyncState;
            socket.EndSend(AR);
        }

    }

}
