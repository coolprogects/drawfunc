﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormClient
{
    public partial class FormClient : Form
    {

        private static Socket _clientSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
        string result;
        Label LBserverStatus;
        
        public FormClient()
        {
            InitializeComponent();
            ConnectToServer();
        }

        private void ConnectToServer()
        {
            if (!_clientSocket.Connected)
            {
                try
                {
                    _clientSocket.Connect(IPAddress.Loopback, 50000);
                    //Clientsocket. Connect (IPAddress.Parse("192.168.11.2"),100)) ;
                }
                catch (SocketException)
                {
                    label9.Text = "not ok";
                }
                if (_clientSocket.Connected)
                {
                    label9.Text = "conection is ok";
                }
            }
        }

        private void SendInfrmaionToServer(string userInfo)
        {
            if (_clientSocket.Connected)
            {
                // 1 convert the form informatino to byte array
                byte[] userData = Encoding.ASCII.GetBytes(userInfo);
                // send data to the server as byte array
                _clientSocket.Send(userData);
            }
        }

        private string ReciveInformationFromServer()
        {
            try
            {
                //_clientSocket.Connect(IPAddress.Loopback, 50000);
                //preper to recive data from the server
                //1.preper byte array to get all the bytes from the servr
                byte[] reciveBuffer = new byte[1024];
                //2.recive the data from the server in to the byte array and
                //return the size of bvtes how recive
                int rec = _clientSocket.Receive(reciveBuffer);
                //3. preper byte array with the size of bytes how recive frm
                //the servr
                byte[] data = new byte[rec];
                //4. copy the byte array how reive in to the byte array with
                //the correct size
                Array.Copy(reciveBuffer, data, rec);
                //5. convert the byte array to Ascii
                string returnFormServer = Encoding.ASCII.GetString(data);
                return returnFormServer;
            }
            catch (Exception e)
            {
                LBserverStatus.Text = "not ok";
            }
            return "";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Color[] colors = { Color.White, Color.Red, Color.Black, Color.Blue, Color.Green };
            Color my_color;
            SendInfrmaionToServer("Rectangle;" + textBox1.Text + ";" + textBox2.Text + ";" + textBox3.Text + ";" + textBox4.Text + ";" + textBox5.Text + ";" + textBox6.Text);
            label8.Text = "Response: ";
            label8.Text += ReciveInformationFromServer();
            if (label8.Text.Equals("Response: The rectangle was successfully painted!!"))
            {
                int R_x, R_y, length, width, isPen = 0;
                int.TryParse(textBox1.Text, out R_x);
                int.TryParse(textBox2.Text, out R_y);
                int.TryParse(textBox3.Text, out length);
                int.TryParse(textBox4.Text, out width);
                int.TryParse(textBox5.Text, out isPen);
                if (textBox6.Text.Equals("White")) my_color = colors[0];
                else if(textBox6.Text.Equals("Red")) my_color = colors[1];
                else if(textBox6.Text.Equals("Black")) my_color = colors[2];
                else if(textBox6.Text.Equals("Blue")) my_color = colors[3];
                else if(textBox6.Text.Equals("Green")) my_color = colors[4];
                else my_color = colors[2];
                if (isPen == 0)
                {
                    p2.Color = my_color;
                    g1.DrawRectangle(p2, R_x, R_y, length, width);
                }
                else
                {
                    sb1.Color = my_color;
                    g1.FillRectangle(sb1, R_x, R_y, length, width);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Color[] colors = { Color.White, Color.Red, Color.Black, Color.Blue, Color.Green };
            Color my_color;
            SendInfrmaionToServer("Ellipse;" + textBox1.Text + ";" + textBox2.Text + ";" + textBox3.Text + ";" + textBox4.Text +  ";" + textBox5.Text + ";" + textBox6.Text);
            label8.Text = "Response: ";
            label8.Text += ReciveInformationFromServer();
            if (label8.Text.Equals("Response: The ellipse was successfully painted!!"))
            {
                int E_x, E_y, length, width, isPen = 0;
                int.TryParse(textBox1.Text, out E_x);
                int.TryParse(textBox2.Text, out E_y);
                int.TryParse(textBox3.Text, out length);
                int.TryParse(textBox4.Text, out width);
                int.TryParse(textBox5.Text, out isPen);
                if (textBox6.Text.Equals("White")) my_color = colors[0];
                else if (textBox6.Text.Equals("Red")) my_color = colors[1];
                else if (textBox6.Text.Equals("Black")) my_color = colors[2];
                else if (textBox6.Text.Equals("Blue")) my_color = colors[3];
                else if (textBox6.Text.Equals("Green")) my_color = colors[4];
                else my_color = colors[2];
                if (isPen == 0)
                {
                    p2.Color = my_color;
                    g1.DrawEllipse(p2, E_x, E_y, length, width);
                }
                else
                {
                    sb1.Color = my_color; 
                    g1.FillEllipse(sb1, E_x, E_y, length, width);
                }
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            string num1 = "", num2 = "", command = "check";
            string[] exercise = textBox7.Text.Split(' ');
            if (exercise.Length == 3)
            {
                command = exercise[1];
                num1 = exercise[0];
                num2 = exercise[2];
            }
            SendInfrmaionToServer("Calculation;" + command + ";" + num1 + ";" + num2);
            label8.Text = "Response: ";
            label8.Text += ReciveInformationFromServer();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            p2.Color = Color.Black;  
            g2.FillRectangle(sb2, 0, 0, 400, 330);
            //g3.DrawRectangle(p2, 450, 330, 400, 330);
            g2.DrawLine(p2, 0, 165, 400, 165);
            g2.DrawLine(p2, 200, 330, 200, 0);
            g2.DrawLine(p2, 350, 180, 400, 165);
            g2.DrawLine(p2, 350, 150, 400, 165);
            g2.DrawLine(p2, 215, 50, 200, 0);
            g2.DrawLine(p2, 185, 50, 200, 0);

            string X = "", Y = "", Incline = "";
            string[] point = textBox8.Text.Split(',');
            
            if (point.Length == 2)
            {
                X = point[0];
                Y = point[1];
                Incline = textBox9.Text;
            }
            SendInfrmaionToServer("Function;" + Incline + ";" + X + ";" + Y);
            label8.Text = "Response: ";
            label8.Text += ReciveInformationFromServer();
            if (label8.Text.Equals("Response: The function draw successfully!!!"))
            {
                int x1, x2 = 200, y1 = 165, y2, x, y, incline, x3 = -200, x4 = 200, y3, y4;
                int.TryParse(X, out x);
                int.TryParse(Y, out y);
                int.TryParse(Incline, out incline);
                y2 = incline * (-x) + y;
                x1 = (-y2) / incline + 200;
                y2 = 165 - y2;
                
                y3 = incline * (x3 - x) + y;
                y4 = incline * (x4 - x) + y;
                y3 = 165 - y3;
                y4 = 165 - y4;
                
                x3 += 200;
                x4 += 200;

                g2.DrawLine(p2, x1, y1, x2, y2);
                if(Math.Abs(y3 - y1) > Math.Abs(y3 - y2)) g2.DrawLine(p2, x2, y2, x3, y3);
                else g2.DrawLine(p2, x1, y1, x3, y3);
                if (Math.Abs(y4 - y1) > Math.Abs(y4 - y2)) g2.DrawLine(p2, x2, y2, x4, y4);
                else g2.DrawLine(p2, x1, y1, x4, y4);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            textBox2.Text = "";
            textBox3.Text = "";
            textBox4.Text = "";
            textBox5.Text = "";
            textBox6.Text = "";
            textBox7.Text = "";
            textBox8.Text = "";
            textBox9.Text = "";
            label9.Text = "";
            label8.Text = "Response: ";
            g1.FillRectangle(sb2, 0, 0, 400, 330);
            g2.FillRectangle(sb2, 0, 0, 400, 330);
        }
    }
}
